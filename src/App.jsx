import React from 'react'
import ReactDOM from 'react-dom'
import { Router, Route, Redirect, hashHistory } from 'react-router'

import IssueList from './components/IssueList.jsx'
import IssueEdit from './components/IssueEdit.jsx'

const NoMatch = () =><p>Page Not Found</p>

const RoutedApp = () => (
  <Router history={hashHistory} >
    <Redirect from="/" to="/issues" />
    <Route path="/issues" component={IssueList} />
    <Route path="/issues/:id" component={IssueEdit} />
    <Route path="*" component={NoMatch} />
</Router> );

ReactDOM.render(<RoutedApp />, document.getElementById('contents'))

if (module.hot) {
  module.hot.accept();
}