import React from 'react'
import { Link } from 'react-router';

class IssueEdit extends React.Component {
  static get propTypes() {
    return {
      params: React.PropTypes.object.isRequired,
    }
  }
  render() {
    return (
      <div>
        <p>This is a placeholder for editing issue {this.props.params.id}.</p>
        <Link to="/issues">Back to issue list</Link>
      </div> 
    )
  }
}

export default IssueEdit