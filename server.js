const MongoClient = require("mongodb").MongoClient
const express = require("express")
const bodyParser = require("body-parser")

const Issue = require('./issue.js')

const app = express()
let db


// Configuration part
if (process.env.NODE_ENV !== 'production') {
  const webpack = require('webpack');
  const webpackDevMiddleware = require('webpack-dev-middleware');
  const webpackHotMiddleware = require('webpack-hot-middleware');
  const config = require('./webpack.config');
  config.entry.app.push('webpack-hot-middleware/client',
    'webpack/hot/only-dev-server');
  config.plugins.push(new webpack.HotModuleReplacementPlugin());
  const bundler = webpack(config);
  app.use(webpackDevMiddleware(bundler, { noInfo: true }));
  app.use(webpackHotMiddleware(bundler, { log: console.log }));
}

app.use(express.static("static"))
app.use(bodyParser.json())

// API routes part
app.get("/api/issues", (req, res) => {
  const filter = {}
  if (req.query.status) {
    filter.status = req.query.status

    db.collection('issues').find({ status: filter.status }).toArray().then(issues => {
      const metadata = { total_count: issues.length }
      res.json({ _metadata: metadata, records: issues })
    }).catch(error => {
      console.log(error)
      res.status(500).json({ message: `Internal Server Error: ${error}` })
    })
  }

  db.collection('issues').find().toArray().then(issues => {
    const metadata = { total_count: issues.length }
    res.json({ _metadata: metadata, records: issues })
  }).catch(error => {
    console.log(error)
    res.status(500).json({ message: `Internal Server Error: ${error}` })
  })
})

app.post("/api/issues", (req, res) => {
  const newIssue = req.body
  newIssue.created = new Date()

  if (!newIssue.status) newIssue.status = 'New'

  const err = Issue.validateIssue(newIssue)
  if (err) {
    res.status(422).json({ message: `Invalid request: ${err}` })
    return
  }

  db.collection('issues').insertOne(newIssue).then(result =>
    db.collection('issues').find({ _id: result.insertedId }).limit(1).next()
  ).then(newIssue => {
    res.json(newIssue)
  }).catch(error => {
    console.log(error)
    res.status(500).json({ message: `Internal Server Error: ${error}` })
  })
})


// Server part
MongoClient.connect('mongodb://localhost/issuetracker').then(connection => {
  db = connection

  app.listen(3000, () => {
    console.log("App started on port 3000")
  })
}).catch(error => {
  console.log('ERROR: ', error)
})